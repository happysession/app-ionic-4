# dev environment

## node (npm )

use [Node Version Manager](https://github.com/creationix/nvm) to handler different versions of node (NPM) if needed

set LTS version (15/02/2019):

- node V10.14.2
- npm v6.4.1

  \$ nvm use --lts

## cordova

install cordova

\$ npm install -g cordova

check version

    $ cordova --version
    > 8.1.2 (cordova-lib@8.1.1)

## ionic 4

install ionic 4

    $ npm install -g ionic

check version

    $ ionic --version
    > 4.10.2

## check installation

check installaltion

    $ ionic info
    > ionic (Ionic CLI)             : 4.10.2
    > Ionic Framework               : @ionic/angular 4.0.1
    >   @angular-devkit/build-angular : 0.12.4
    >   @angular-devkit/schematics    : 7.2.4
    >   @angular/cli                  : 7.2.4
    >   @ionic/angular-toolkit        : 1.3.0
    > Cordova:
    >   cordova (Cordova CLI) : 8.1.2 (cordova-lib@8.1.1)
    >   Cordova Platforms     : browser 5.0.4, ios 4.5.5
    >   Cordova Plugins       : cordova-plugin-ionic-keyboard 2.1.3, cordova-plugin-ionic-webview 2.3.3, (and 5 other plugins)
    > System:

    >   ios-deploy : 2.0.0
    >   ios-sim    : 5.0.13
    >   NodeJS     : v10.14.2 (/Users/durand/.nvm/versions/node/v10.14.2/bin/node)
    >   npm        : 6.4.1

you're ready to start

## Caveats

You need to deploy on iOS 10.3, be careful, ionic 4 comes with cordova-plugin-ionic-webview version 3.x.x wicth is not compatable with iOS < 11. [@see Plugin Requirements](https://github.com/ionic-team/cordova-plugin-ionic-webview)

# get started

## CLI

create a empty app from _tabs_ pattern

    $ ionic start myApp tabs
    $ cd myApp

## add GoogleMaps pluging

    $ ionic cordova plugin add cordova-plugin-googlemaps --variable API_KEY_FOR_ANDROID="MY_KEY" --variable API_KEY_FOR_IOS="MY_KEY"
    $ npm install @ionic-native/core@beta @ionic-native/google-maps@beta

check _package.json_

    "cordova-plugin-googlemaps": "~2.5.0",
    "@ionic-native/core": "^5.0.0",

check config.xml

    <plugin name="cordova-plugin-googlemaps" spec="~2.5.0">
        <variable name="API_KEY_FOR_ANDROID" value="MY_KEY" />
        <variable name="API_KEY_FOR_IOS" value="MY_KEY" />
        <variable name="PLAY_SERVICES_VERSION" value="15.0.1" />
        <variable name="ANDROID_SUPPORT_V4_VERSION" value="27.+" />
        <variable name="LOCATION_WHEN_IN_USE_DESCRIPTION" value="This app wants to get your location while this app runs only." />
        <variable name="LOCATION_ALWAYS_USAGE_DESCRIPTION" value="This app wants to get your location always, even this app runs in background." />
    </plugin>

_Nota Bene_ :

you can verify webview plugin version in config.xml (need to be 2.x.x for iOS 10.3)

    <plugin name="cordova-plugin-ionic-webview" spec="^2.3.3" />

## code

### .ts

you neeed to implement _implements \_OnInit_ to defere your execution after page loading.

    export class Tab2Page implements OnInit {
        map: GoogleMap;

        constructor(private platform: Platform) {}

        async ngOnInit() {
            // sync on ionic (cordova end loading)
            await this.platform.ready();
            // then load map
            await this.loadMap();
        }

        loadMap() {
            // get map by selector (HTML selector)
            this.map = GoogleMaps.create("map_canvas", {
            camera: {
                target: {
                lat: 43.0741704,
                lng: -89.3809802
                },
                zoom: 18,
                tilt: 30
            }
            });
        }
    }

### .html

simple

    <ion-content>
        <div id="map_canvas">
        </div>
    </ion-content>

### .scss

`don't forget width, height properties for CSS map selector`

    #map_canvas {
        width: 100%;
        height: 100%;
        border: 1px solid #4ab4e6;
    }

# Tests

## run in browser

    $ ionic cordova run browser

## run iOS

build

    $ ionic cordova build ios

run from XCode (check target version if iOS 10.3)
