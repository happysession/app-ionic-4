import { Component, OnInit, ViewChild } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  Marker,
  GoogleMapsEvent,
  GoogleMapsMapTypeId
} from '@ionic-native/google-maps';
import { Platform, IonSlides } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { MapType } from '@angular/compiler';

@Component({
  selector: 'app-map',
  templateUrl: 'map.page.html',
  styleUrls: ['map.page.scss']
})
export class MapPage implements OnInit {
  slideOpts = {
    effect: 'flip'
  };
  map: GoogleMap;
  sessions: any = [];
  currentArea = {
    code: 'BZH',
    name: 'Bretagne',
    mapConfig: { zoom: 7, lat: 48.5, lon: -3.2 }
  };
  currentDay = 0;
  currentDate: Date = new Date();
  segment = 'MAP';

  @ViewChild('daysSlider') slider: IonSlides;

  constructor(private platform: Platform, private api: ApiService) {}

  async ngOnInit() {
    await this.platform.ready();
    await this.loadMap();
  }

  loadMap() {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 48.184944,
          lng: -2.755514
        },
        zoom: 7,
        tilt: 30
      },
      mapType: GoogleMapsMapTypeId.SATELLITE
    });
    this.loadMarkers();
  }

  loadMarkers() {
    this.api
      .getBestSessionsByRegion(this.currentArea.code, this.currentDay)
      .subscribe(sessions => {
        this.sessions = sessions;

        for (const session of this.sessions) {
          this.currentDate = session.exeDate;
          const size = (15 + session.bestWaveContext.waveHeight * 5).toFixed(0);
          const marker: Marker = this.map.addMarkerSync({
            position: {
              lat: session.spot.location.latitude,
              lng: session.spot.location.longitude
            },

            title: session.spot.name,
            icon: {
              url:
                'www/assets/markers/location-' +
                this.getMarkerColor(session) +
                '.png',
              size: {
                width: size,
                height: size
              }
            }
          });

          marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(this.onMarkerClick);
          marker.on(GoogleMapsEvent.INFO_CLICK).subscribe(this.onMarkerClick);
        }
      });
  }
  private onMarkerClick(params: any) {}

  updateMap() {}

  clearMarkers() {
    this.map.clear();
  }

  changeDay(index: number) {
    // this.hasAreaChange = false;
    this.currentDay = this.currentDay + index;
    this.slider.slideTo(this.currentDay); // => implicit call to onDayChanged
  }

  onDayChanged() {
    this.clearMarkers();
    this.slider.getActiveIndex().then(activIndex => {
      if (activIndex < 7) {
        this.currentDay = activIndex;
        this.loadMarkers();
      } else {
        this.currentDay = activIndex - 1;
      }
    });
  }

  private getMarkerColor(session) {
    const w: number = session.waveAsset;
    let color: String = '';
    if (w > 0.9) {
      color = 'green';
    } else if (w > 0.8) {
      color = 'lightgreen';
    } else if (w > 0.7) {
      color = 'lightyellow';
    } else if (w > 0.6) {
      color = 'yellow';
    } else if (w > 0.5) {
      color = 'darkyellow';
    } else if (w > 0.4) {
      color = 'orange';
    } else if (w > 0.3) {
      color = 'darkorange';
    } else if (w > 0.1) {
      color = 'red';
    } else {
      color = 'grey';
    }
    return color;
  }
}
