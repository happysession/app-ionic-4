import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-buoy',
  templateUrl: 'buoy.page.html',
  styleUrls: ['buoy.page.scss']
})
export class BuoyPage implements OnInit {
  constructor(private platform: Platform, private api: ApiService) {}

  async ngOnInit() {
    await this.platform.ready();
  }
}
