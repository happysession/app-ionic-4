import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuoyPage } from './buoy.page';

describe('BuoyPage', () => {
  let component: BuoyPage;
  let fixture: ComponentFixture<BuoyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuoyPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuoyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
