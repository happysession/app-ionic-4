import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-webcam',
  templateUrl: 'webcam.page.html',
  styleUrls: ['webcam.page.scss']
})
export class WebcamPage implements OnInit {
  constructor(private platform: Platform, private api: ApiService) {}

  async ngOnInit() {
    await this.platform.ready();
  }
}
