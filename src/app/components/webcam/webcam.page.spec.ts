import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcamPage } from './webcam.page';

describe('WebcamPage', () => {
  let component: WebcamPage;
  let fixture: ComponentFixture<WebcamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WebcamPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebcamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
