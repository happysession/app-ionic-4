import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-profil',
  templateUrl: 'profil.page.html',
  styleUrls: ['profil.page.scss']
})
export class ProfilPage implements OnInit {
  constructor(private platform: Platform, private api: ApiService) {}

  async ngOnInit() {
    await this.platform.ready();
  }
}
