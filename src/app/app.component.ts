import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Environment } from '@ionic-native/google-maps/ngx';
import { HSEnv } from './services/hs.env';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    public hsEnv: HSEnv,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // use in ionic browser mode
      Environment.setEnv({
        API_KEY_FOR_BROWSER_RELEASE: this.hsEnv.gmapKey,
        API_KEY_FOR_BROWSER_DEBUG: this.hsEnv.gmapKey
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.translate.setDefaultLang('en');

      const lng: string = navigator.language.split('-')[0];
      if (lng === 'fr') {
        this.translate.use(lng);
        this.hsEnv.lng = lng;
      }
    });
  }
  openLogin() {}
}
