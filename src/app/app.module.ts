import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ApiService } from './services/api.service';

import {
  TranslateLoader,
  TranslateModule,
  TranslatePipe
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HSEnv } from './services/hs.env';
import { HSUtils } from './services/hs.utils';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule
  ],
  providers: [
    ApiService,
    HSEnv,
    HSUtils,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}
/*
 It seems to be an issue with 'ionic cordova run browser', getTranslation() is OK in root module but is empty as child in other modules.
 NB : TranslateModule.forChild() is correct for iOS Android test

export function createTranslateLoader(http: HttpClient) {
  console.log('load translations');
  const thl = new TranslateHttpLoader(http, '/assets/i18n/', '.json');
  thl
    .getTranslation('en')
    .subscribe(
      trl => console.log('---->' + JSON.stringify(trl)),
      error => console.error(error)
    );
  return thl;
}
 */
