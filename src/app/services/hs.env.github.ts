import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HSEnv {
  app: String = 'Happy Session';
  version: String;
  lng: String = 'en';
  vertioUrl = 'https://vertio.fr';
  gmapKey = 'AIzaSyDVsYsjILP7B5iRzTwiqfpKlIGyQY4XLXc';
}
