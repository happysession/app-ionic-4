import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HSEnv } from './hs.env';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private hsEnv: HSEnv, public http: HttpClient) {}

  getBestSessionsByRegion(area, day) {
    return this.http.get(
      this.hsEnv.vertioUrl + '/context/best/area/' + area + '/' + day
    );
  }
}
