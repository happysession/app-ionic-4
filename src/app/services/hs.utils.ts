import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class HSUtils {
  selectTabBS: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor() {
    this.selectTabBS.next(0);
  }

  getSelectTabBS() {
    return this.selectTabBS;
  }
}
